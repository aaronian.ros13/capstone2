const mongoose = require("mongoose");

const user_schema = new mongoose.Schema({
    email : {
        type : String,
        required : [true, "First Name is required"]
    },
    password : {
        type : String,
        required : [true, "Last Name is required"]
    },
    isAdmin : {
        type: Boolean,
        default: true,
        required: true
    }
})



module.exports = mongoose.model("User", user_schema);