const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');


//Find All user
module.exports.getAllUser = (request, response) => {
    return User.find({}).then(result => {
        return response.send(result);
    })
}

//User registration
module.exports.registerUser = (request_body) => {
	let new_user = new User({
		email: request_body.email,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save().then((registered_user, error) =>{
		if(error){
			return {
				message: error.message
			};
		}

		return {
			message: 'Successfully registereda user!'
		};
	}).catch(error => console.log(error));
}

//User authentication
module.exports.authenticateUser = (request, response) => {
    return User.findOne({email: request.body.email}).then(result => {
        // Checks if a user is found with an existing email
        if(result == null){
            return response.send({
                message: "The user isn't registered yet."
            }) 
        }

        // If a user was found with an existing email, then check if the password of that user matched the input from the request body
        const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

        if(isPasswordCorrect){
            // If the password comparison returns true, then respond with the newly generated JWT access token.
            return response.send({
                accessToken: auth.createAccessToken(result),
                userId: result._id
            });
                
        } else {
            return response.send({
                message: 'Your password is incorrect.'
            })
        }
    }).catch(error => response.send(error));
}

module.exports.getProfile = (request_body) => {
    return User.findOne({_id: request_body.id}).then((user, error) => {
        if(error){
            return {
                message: error.message 
            }
        }

        user.password = "";

        return user;
    }).catch(error => console.log(error));
}

module.exports.setAdmin = (request, response) => {
    const id = request.params.id;

    User.findByIdAndUpdate(id, { isAdmin: true }, { new: true }).then((user) => {
            if (!user) {
                return response.status(404).json({ message: 'User not found' });
            }
            return response.json({ message: 'User has be set to admin', user });
        })
        .catch((error) => {
            return response.status(500).json({ message: error.message });
        });
}

const checkOut = async (request, response) => {
    try {
        if (request.user.isAdmin) {
            return response.send('Action Forbidden');
        }

        let user = await User.findById(request.user.id);
        if (!user) {
            return response.status(404).json({ message: 'User not found' });
        }

        let product = await Product.findById(request.body.productId);
        if (!product) {
            return response.status(404).json({ message: 'Product not found' });
        }

        if (!user.checkouts) {
            user.checkouts = [];
        }

        let new_checkout = {
            productId: request.body.productId
        }

        user.checkouts.push(new_checkout);

        let isUserUpdated = await user.save().then(updated_user => true).catch(error => error.message);

        if (isUserUpdated !== true) {
            return response.status(500).json({ message: isUserUpdated });
        }

        if (!product.checkouts) {
            product.checkouts = [];
        }

        let new_checkout_product = {
            userId: request.user.id
        }

        product.checkouts.push(new_checkout_product);

        let isProductUpdated = await product.save().then(updated_product => true).catch(error => error.message);

        if (isProductUpdated !== true) {
            return response.status(500).json({ message: isProductUpdated });
        }

        if (isUserUpdated && isProductUpdated) {
            return response.status(200).json({ message: 'Checkout successful!' });
        }
    } catch (error) {
        console.error('Checkout error:', error);
        response.status(500).json({ message: 'Checkout failed' });
    }
}








