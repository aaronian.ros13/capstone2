const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');


router.get('/all', (request, response) => {
    UserController.getAllUser(request, response);
})

router.post('/register', (request, response) => {
	UserController.registerUser(request.body).then((result) => {
		response.send(result);
	})
})

router.post('/authenticate', (request, response) => {
	UserController.authenticateUser(request, response);
})

router.post('/details', auth.verify, (request, response) => {
	UserController.getProfile(request.body).then((result) => {
		response.send(result);
	})
})


router.post('/checkout', auth.verify, (request, response) => {
	UserController.checkOut(request, response);
})

router.put('/:id/set', auth.verify, auth.verifyAdmin, (request, response) => {
    UserController.setAdmin(request, response);
})

module.exports = router;