const express = require('express');
const router = express.Router();
const OrderController = require('../controllers/OrderController.js'); 
const auth = require('../auth');



router.post('/create', auth.verify, (request, response) => {
	OrderController.createOrder(request, response);
}) 

router.get('/retrieve', auth.verify, (request, response) => {
	OrderController.retrieveUserOrders(request, response);
}) 

router.get('/getall', auth.verify, (request, response) => {
	OrderController.getAllOrders(request, response);
}) 
	

module.exports = router;

